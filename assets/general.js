$(document).ready(function(){
  var color = $(".custume-swatch .swatch-element > label").css("background-color");
  console.log(color)
  setTimeout(function(){

    $(".product-vendor, .product-type .stamped-fa.stamped-fa-star").css("color",color);
    $(".product-form__cart-submit").css("background-color",color);
    $(".qty__with__addcart, .qty-inner-option").css("border-color",color)
    $(".product-detail-section.medium-half .accordion .accordion-title.active").css("color",color);
    $(".accordion-title").click(function(){
   		 var color = $(".custume-swatch .swatch-element > label").css("background-color");
      $(".product-detail-section.medium-half .accordion .accordion-title.active").css("color",color);
      if(!$(this).hasClass("active")){
        $(this).removeAttr("style");
      }
  })

  }, 3000);

  ;(function($){
    $.fn.equalHeight = function (option) {
      var $this = this
      var get_height = function(){
        var maxheight=0;
        $this.css("height","")
        $this.each(function(){
          maxheight = $(this).height() > maxheight ? $(this).height() : maxheight;
        })
        $this.height(maxheight)
      }
      var init =function(){
        get_height()
        $(window).bind("resize",get_height)
      }
      $this.destroy = function(){
        $this.css("height","")
        $(window).unbind("resize",get_height)
      }
      init()
      return this
    } 
  })(jQuery)
  $(".logo-bar .animation-height").equalHeight();
  // remove all .active classes when clicked anywhere
  $(function() {
    $("#collection-toogle").on("click", function(e) {
      $(".inner-collection-sort").toggleClass("active");
    });
    $(document).on("click", function(e) {
      if ($(e.target).is("#collection-toogle, .inner-collection-sort") === false) {
        $(".inner-collection-sort").removeClass("active");
      }
    });
  });

  $(function() {
    $(".collection-tag #collection-toogle").on("click", function(e) {
      $(".collection-tag").toggleClass("active");
    });
    $(document).on("click", function(e) {
      if ($(e.target).is("#collection-toogle, .collection-tag") === false) {
        $(".collection-tag").removeClass("active");
      }
    });
  });
  (function($, window){
    var arrowWidth = 60;
    var i=0;
    $.fn.resizeselect = function(settings) {  
      console.log(i);
      i++
      return this.each(function() {
        $(this).change(function(){
          var $this = $(this);
          // create test element
          var text = $this.find("option:selected").text();
          var $test = $("<span>").html(text).css({
            "font-size": $this.css("font-size"), // ensures same size text
            "visibility": "hidden" 							 // prevents FOUC
          });
          // add to body, get width, and get out
          $test.appendTo($this.parent());
          var width = $test.width();
          $test.remove();
          // set select width
          $this.width(width + arrowWidth);
          // run on start
        }).change();
      });
    };
    // run by default
    $("select.vendor__input").resizeselect();
    // $("select.short__by").resizeselect();
  })(jQuery, window);

  let params = (new URL(document.location)).searchParams;
  let Street = params.get('q');
  console.log(Street);
  $("body").addClass(Street);
  jQuery(function($){
    $(".template-collection ul a")
    .click(function(e) {
      var link = $(this);
      var item = link.parent("li");
      if (item.hasClass("active")) {
        item.removeClass("active").children("a").removeClass("active");
      } else {
        item.addClass("active").children("a").addClass("active");
      }
      if (item.children("ul").length > 0) {
        var href = link.attr("href");
        link.attr("href", "#");
        setTimeout(function () { 
          link.attr("href", href);
        }, 300);
        e.preventDefault();
      }
    })
    .each(function() {
      var link = $(this);
      if (link.get(0).href === location.href) {
        link.addClass("active").parents("li").addClass("active");
        link.addClass("active").parents(".collection-tag").addClass("clear_active");
        link.addClass("active").parents(".inner-collection-sort").addClass("clear_active");
        return false;
      }
    });
  });
})