$(function() {
  
  function try_add_selected_one(dest,src, value){
    let $added = false;
  	if ( !$('div#color-pad-'+dest).data('image') ) {
      $('div#color-pad-'+dest+' img').attr('src',src);
      $('div#color-pad-'+dest).data('image','done');
      $('div#selected-shade-'+dest+' span#shade-'+dest+'-color').html(value);
      
        $('input[name="properties[shade-'+dest+']"').val(value);
      
      $added = true;
    }
    return $added;
  
  }
  
  function clear_selected_one(dest){
      
     $('input[name="properties[shade-'+dest+']"').val(''); 
  }
  
  $('div.colorpad-box').click(function(e){
  	let $imgobj = $(this).find('img');
    if ( $(this).data('image') == 'done' ) {
      $(this).data('image','');
      $('.swath :radio').prop('checked', false);
    $("input:radio").attr("checked", false);
  
      $imgobj.attr('src','data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNgYAAAAAMAASsJTYQAAAAASUVORK5CYII=');
    }
    let $index = $(this).data('index');
    $('div#selected-shade-' + $index +' span#shade-' + $index + '-color').html('Empty');
    clear_selected_one($index);
    
  });
  
  //$('.swatch :radio').change(function() {
  $('#yx-colorpad-options').on('change','.swatch :radio',function(){
    var optionIndex = $(this).closest('.swatch').attr('data-option-index');
    var optionValue = $(this).val();
    var color_image_src = $(this).data('image-src');
    var available = !$(this).hasClass('sold-out');
    
    if ( !available ) {
//     	alert('Sorry , sold out');
      return;
    }
   
    var $added = false;
    $added = try_add_selected_one('1',color_image_src,optionValue);
    if ( !$added ) {
      $added = try_add_selected_one('2',color_image_src,optionValue);
    }
    if ( !$added ) {
      $added = try_add_selected_one('3',color_image_src,optionValue);
    }
    if ( !$added ) {
      $added = try_add_selected_one('4',color_image_src,optionValue);
    }
    if ( !$added ) {
      $added = try_add_selected_one('5',color_image_src,optionValue);
    }
    if ( !$added ) {
      $added = try_add_selected_one('6',color_image_src,optionValue);
    }
    if ( !$added ) {
      $added = try_add_selected_one('7',color_image_src,optionValue);
    }
    if ( !$added ) {
      $added = try_add_selected_one('8',color_image_src,optionValue);
    }
    
   
    // update real selected color pade in order to put into shopping cart
    
  
    /*
    $(this)
      .closest('form')
      .find('.single-option-selector')
      .eq(optionIndex)
      .val(optionValue)
      .trigger('change');*/
  }); 
});